'use strict';

var gulp = require('gulp');
var replace = require('gulp-replace');
var ts = require('gulp-typescript');

gulp.task('default', function()
{
    var doReplace = !process.env['ALERT_BUILDER_SKIP_REPLACE']; // if this env var is there, skip replacing the paths
    if(doReplace)
    {
        // otherwise, use the vars (which might be fetched from the environment)
        var queryUrl = process.env['ALERT_BUILDER_INFLUXDB_QUERY_URL'] || "https://influxdb.example.com/query";
        var kappaUrl = process.env['ALERT_BUILDER_KAPACITOR_API_URL'] || "https://kapacitor.example.com/kapacitor/v1/";
    }

    var typescriptOptions =
    {
        "noImplicitAny": true,
        "preserveConstEnums": true,
        "outFile": "alert-builder.js"
    };

    // typescript compile (always do this taask)
    var tasks = gulp.src('src/**/*.ts').pipe(ts(typescriptOptions));

    // if we need to replace strings, do it here
    if(doReplace)
    {
        tasks = tasks
            .pipe(replace('"/query"', '"' + queryUrl + '"'))
            .pipe(replace('"/kapacitor/v1/"', '"' + kappaUrl + '"'))
    }

    // move results to this directory
    return tasks.pipe(gulp.dest('webroot/'));
});
