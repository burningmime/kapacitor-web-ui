/// <reference types="jquery" />

namespace Kapacitor
{
    const urlKapacitorApi = "/kapacitor/v1/";

    export interface TaskList { tasks: Task[]; }
    export interface Task
    {
        // note all these are optional so we can use the PATCH method to change only some of them
        created?:string;
        dbrps?:DbrpsWrapper[];
        dot?:string;
        error?:string;
        executing?:boolean;
        id?:string;
        "last-enabled"?:string;
        link?:LinkWrapper;
        modified?:string;
        script?:string;
        stats?:StatsWrapper;
        status?:string;
        type?:string;
        vars?:TaskVars;
    }

    export interface DbrpsWrapper { db:string; rp:string; }
    export interface LinkWrapper { rel:string; href:string; }
    export interface TaskVars { [name:string] : TaskVar; }
    export interface TaskVar { value?:any; type:string; }
    export interface StatsWrapper { "task-stats":TaskStats; "node-stats":NodeStatMap; }
    export interface TaskStats { throughput:number; }
    export interface NodeStatMap { [name:string]:NodeStats }
    export interface NodeStats
    {
        alerts_triggered?:number;
        avg_exec_time_ns?:number;
        collected?:number;
        crits_triggered?:number;
        emitted?:number;
        infos_triggered?:number;
        oks_triggered?:number;
        warns_triggered?:number;
        points_written?:number;
    }

    export function api(path:string, callback:(data:any)=>void, onError?:(url:string, xhr:JQueryXHR)=>void, method?:string, body?:any, returnString:boolean = false)
    {
        onError = onError || Utils.defaultApiErrorHandler;
        let url = urlKapacitorApi + path;
        let settings:JQueryAjaxSettings =
        {
            async: true,
            cache:false,
            dataType:"json",
            error(xhr, status, err) { console.log(status); console.log(err); onError(url, xhr); },
            success: callback
        };
        if(method) { settings.method = method; }
        settings.dataType = returnString ? "text" : "json";
        if(body)
        {
            settings.data = JSON.stringify(body);
            settings.contentType = "'application/json; charset=utf-8";
        }
        $.ajax(url, settings);
    }
}
