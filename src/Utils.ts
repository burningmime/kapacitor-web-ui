/// <reference types="jquery" />

namespace Utils
{
    export function idToTaskName(id:string):string
    {
        if(/_[0-9a-f]{16}$/.test(id)) // old alert builder created tasks with IDs like this
            return id.substr(0, id.length - 17).replace(/_/g, " ");
        else
            return id.replace(/_/g, " ");
    }

    export function queryString(name:string)
    {
        let regex = new RegExp("[?&]" + name.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)");
        let results = regex.exec(window.location.href);
        if(!results) return null;
        if(!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    export function genGuid():string
    {
        const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        return s4() + s4() + s4() + s4();
    }

    // wrap the ugly looking cast to <any> here
    export function modal(obj:JQuery, action:string)
    {
        (<any>obj).modal(action);
    }

    // assumes there are dialogError and dialogErrorBody elements on the page and uses them to show a default error dialog
    export function defaultApiErrorHandler(url:string, xhr:JQueryXHR)
    {
        let div = document.createElement("div");
        let header = document.createElement("b");
        header.innerText = "Error accessing " + url;
        div.appendChild(header);
        let text = document.createElement("p");
        text.innerText = xhr.statusText || "unknown errror (probably a certificate error; check your browser logs)";
        div.appendChild(text);
        if(xhr.responseText)
        {
            let respText = document.createElement("pre");
            respText.style.maxWidth = "400px";
            respText.style.maxHeight = "300px";
            respText.style.overflow = "scroll";
            respText.innerText = xhr.responseText;
            div.appendChild(respText);
        }
        errorDialogHtml("Error accessing API", div);
    }

    export function errorDialogText(title:string, text:string)
    {
        let elem = document.createElement("pre");
        elem.style.maxWidth = "400px";
        elem.style.maxHeight = "300px";
        elem.style.overflow = "scroll";
        elem.innerText = text;
        errorDialogHtml(title, elem)
    }

    export function errorDialogHtml(title:string, elem:HTMLElement)
    {
        let dialogErrorTitle = document.getElementById("dialogErrorTitle");
        dialogErrorTitle.innerText = title;
        let dialogErrorBody = document.getElementById("dialogErrorBody");
        dialogErrorBody.innerHTML = "";
        dialogErrorBody.appendChild(elem);
        Utils.modal($("#dialogError"), "show");
    }

    export function textColumn(row: HTMLTableRowElement, text:string)
    {
        Utils.column(row, document.createTextNode(text));
    }

    export function column(row: HTMLTableRowElement, inner:Node)
    {
        let col = document.createElement("td");
        col.appendChild(inner);
        row.appendChild(col);
    }

    // from: https://stackoverflow.com/questions/1293147/javascript-code-to-parse-csv-data
    export function parseCsv(csv:string)
    {
        // Create a regular expression to parse the CSV values.
        let objPattern = new RegExp(
            (
                "(,|\\r?\\n|\\r|^)" +
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
                "([^\",\\r\\n]*))"
            ),
            "gi"
        );

        let arrData:string[][] = [];
        arrData.push([]);
        let arrMatches:RegExpExecArray;
        while (arrMatches = objPattern.exec(csv))
        {
            // delimiter
            let strMatchedDelimiter = arrMatches[1];
            if(strMatchedDelimiter.length && strMatchedDelimiter !== ",")
                arrData.push([]);

            // value
            let strMatchedValue:string;
            if (arrMatches[2]) // quoted string
                strMatchedValue = arrMatches[2].replace(new RegExp( "\"\"", "g" ), "\"");
            else // non-quoted value
                strMatchedValue = arrMatches[3];

            arrData[arrData.length - 1].push(strMatchedValue);
        }

        return arrData;
    }

    export function foreach(obj:any, callback:(key:string, value:any)=>void)
    {
        for(let key in obj)
            if(obj.hasOwnProperty(key))
                callback(key, obj[key]);
    }

    export function escapeHtml(s:string):string
    {
        return s.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }

    export function clearChildren(elem:HTMLElement)
    {
        while(elem.lastChild)
            elem.removeChild(elem.lastChild);
    }
}
