/// <reference path="influx.ts" />

class MeasurementsPage
{
    private measurementsText:HTMLElement;

    private showMeasurements(csv:string)
    {
        let results:string[] = [];
        let isFirst = true;
        let pattern = /[^,]*,[^,]*,([^\n]*)\n/g;
        let match:RegExpExecArray;
        while((match = pattern.exec(csv)) !== null)
        {
            if(isFirst) { isFirst = false; continue; }
            results.push(match[1]);
        }
        results.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
        this.measurementsText.innerText = results.join("\n");
    }

    private errorHandler(url:string, xhr:JQueryXHR)
    {
        let errorText = "Error accessing " + url + "\n";
        try { errorText += xhr.statusText + "\n"; } catch(_) { }
        try { errorText += xhr.responseText + "\n"; } catch(_) { }
        errorText += "(check browser logs if this seems strange and mysterious -- it could be a certificate error)";
        this.measurementsText.style.color = "red";
        this.measurementsText.innerText = errorText;
    }

    public init()
    {
        let db = Utils.queryString("db");
        this.measurementsText = document.getElementById("measurementsText");
        Influx.queryCsv("SHOW MEASUREMENTS ON " + db, csv => this.showMeasurements(csv), (url, xhr) => this.errorHandler(url, xhr));
    }
}
