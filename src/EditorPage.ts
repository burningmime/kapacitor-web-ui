/// <reference types="ace" />
/// <reference types="jquery" />
/// <reference path="utils.ts" />
/// <reference path="kapacitor.ts" />
/// <reference path="tickmode.ts" />
/// <reference path="autocomplete.ts" />

class EditorPage
{
    private editor : AceAjax.Editor;
    private task:Kapacitor.Task;
    private lastSavedScript:string; // store last saved script so we can check if it's been modified

    // applies teh given changes to Kapacitor as a PATCH. See: https://docs.influxdata.com/kapacitor/v1.2/api/api/#tasks
    private patch(changes:Kapacitor.Task, callback?:(task:Kapacitor.Task)=>void)
    {
        Kapacitor.api("tasks/" + this.task.id,
            t => { this.updateTask(t, false); if(callback) callback(t); },
            EditorPage.mainErrorHandler,
            "PATCH", changes);
    }

    // Callback when a new task a recieved. The stats/toolbox panel makes the same requests, but we don't want to change
    // the user's script, etc, every time we get a callback from that, so this method is only called on the initial load
    // and after any submitted patches.
    private updateTask(task:Kapacitor.Task, isInitialLoad:boolean)
    {
        this.task = task;
        this.lastSavedScript = task.script;
        // setting the session will clear the undo manager among other things
        if(isInitialLoad)
            this.editor.session.setValue(task.script);
        else
            this.editor.setValue(task.script);
        this.editor.clearSelection();
        this.setEnabledStyle();
        this.setRuntimeErrors(task.error);

        // toolbox panel stuff
        this.showMeasurementsLink.href = "measurements.html?db=" + task.dbrps[0].db;
        this.statsTextBlock.innerText = task.dot;
        this.updateHttpEndpoints();
        this.requestStats(true);
    }

    // (Tries to) save the user script. If the task is enabled, it needs to disable and re-enable the task
    // for it to refresh and pick up new changes.
    public doSave()
    {
        let script = this.editor.getValue();
        if(this.lastSavedScript !== script)
        {
            if(this.task.status ===  "enabled")
            {
                // need to disable then enable it to refresh it
                this.patch({ script: script, status: "disabled" },
                    t => this.patch({ status: "enabled" }));
            }
            else
            {
                this.patch({ script: script });
            }
        }
    }

    //==================================================================================================================
    // TOOLBOX PANEL
    //==================================================================================================================

    private layoutLeft:HTMLElement;
    private layoutRight:HTMLElement;
    private debugToolboxPanel:HTMLElement;
    private errorsToolboxPanel:HTMLElement;
    private debugToolboxOpen = false;
    private errorsToolboxOpen = false;


    public toggleDebugToolbox()
    {
        if(this.debugToolboxOpen)
        {
            this.debugToolboxOpen = false;
            this.debugToolboxPanel.hidden = true;
            this.closeToolboxPanel();
        }
        else if(this.errorsToolboxOpen)
        {
            this.errorsToolboxOpen = false;
            this.debugToolboxOpen = true;
            this.errorsToolboxPanel.hidden = true;
            this.debugToolboxPanel.hidden = false;
            this.requestStats(true);
        }
        else
        {
            this.debugToolboxOpen = true;
            this.debugToolboxPanel.hidden = false;
            this.openToolboxPanel();
            this.requestStats(true);
        }
    }

    public toggleErrorsToolbox()
    {
        if(this.debugToolboxOpen)
        {
            this.debugToolboxOpen = false;
            this.errorsToolboxOpen = true;
            this.debugToolboxPanel.hidden = true;
            this.errorsToolboxPanel.hidden = false;
        }
        else if(this.errorsToolboxOpen)
        {
            this.errorsToolboxOpen = false;
            this.errorsToolboxPanel.hidden = true;
            this.closeToolboxPanel();
        }
        else
        {
            this.errorsToolboxOpen = true;
            this.errorsToolboxPanel.hidden = false;
            this.openToolboxPanel();
        }
    }

    private openToolboxPanel()
    {
        this.layoutLeft.classList.remove("layoutLeft_panelClosed");
        this.layoutLeft.classList.add("layoutLeft_panelOpen");
        this.layoutRight.classList.remove("layoutRight_panelClosed");
        this.layoutRight.classList.add("layoutRight_panelOpen");
        this.editor.resize();
    }

    private closeToolboxPanel()
    {
        this.layoutLeft.classList.remove("layoutLeft_panelOpen");
        this.layoutLeft.classList.add("layoutLeft_panelClosed");
        this.layoutRight.classList.remove("layoutRight_panelOpen");
        this.layoutRight.classList.add("layoutRight_panelClosed");
        this.editor.resize();
    }

    //==================================================================================================================
    // DEBUG TOOLBOX
    //==================================================================================================================

    private static STATS_UPDATE_FREQUENCY_SECONDS = 5; // how often to update the toolbox stats and http endpoints
    private statsLoopActive = false;
    private showMeasurementsLink:HTMLAnchorElement;
    private statsTextBlock:HTMLElement;

    // starts or continues a stats request loop (this requests the main task for the DOT graph, and may also make requests
    // to any httpOut endpoints in the TICK script)
    private requestStats(isInitialRequest:boolean)
    {
        // update toolbox only if
        //   - toolbox panel is open (duh)
        //   - the task is running (if the task isn't running, toolbox aren't beign updated)
        //   - only if we don't already have an active "loop" going on (so we don't end up with cascading requests)
        if(this.debugToolboxOpen && this.task.executing && (!isInitialRequest || !this.statsLoopActive))
        {
            this.statsLoopActive = true;
            Kapacitor.api("tasks/" + this.task.id, t => this.updateStats(t), (_, xhr) => this.statsErrorHandler(xhr));
            // if we have any http endpoints we're watching, check those too
            for(let endpoint in this.httpEndpointColumns)
            {
                if(this.httpEndpointColumns.hasOwnProperty(endpoint))
                {
                    let col = this.httpEndpointColumns[endpoint];
                    Kapacitor.api("tasks/" + this.task.id + "/" + endpoint,
                        res => EditorPage.endpointDataHandler(res, col),
                        (_, xhr) => EditorPage.endpointErrorHandler(xhr, col));
                }
            }
        }
        else
        {
            this.statsLoopActive = false;
        }
    }

    // Updates teh main "execution graph" (DOT graph) in the toolbox panel. Also continues the request loop to keep
    // updating stats.
    // TODO there's a javascript port of graphviz if we want to actually show the image
    private updateStats(task:Kapacitor.Task)
    {
        if(task.dot)
        {
            this.statsTextBlock.innerText = task.dot;
            // extra executing check here in case for some reason the task has stopped executing but the cached this.task still thinks it's executing
            if(task.executing)
            {
                // the callback function here will make additional checks to prevent too many requests
                setTimeout(() => this.requestStats(false), EditorPage.STATS_UPDATE_FREQUENCY_SECONDS * 1000);
            }
            else
            {
                this.statsLoopActive = false;
            }
        }
        else
        {
            this.statsLoopActive = false;
        }

        // if there are runtime erros, this is generally when they would show up
        this.setRuntimeErrors(task.error);
    }

    // error handler called when a toolbox stats request fails. we don't want to bother the user with a big modal dialog, so just put
    // the error message straight into the text
    private statsErrorHandler(xhr:JQueryXHR)
    {
        let errorString:string = "Error getting toolbox:\n\n";
        try { errorString += xhr.responseJSON.error; }
        catch(_) { errorString += xhr.statusText || "Unknown error (maybe a certificate error; check browser logs)"; }
        this.statsTextBlock.innerText = errorString;
        this.statsLoopActive = false;
    }

    //==================================================================================================================
    // HTTP ENDPOINTS VIEW
    //==================================================================================================================

    private httpEndpointsContainer:HTMLElement;
    private httpEndpointColumns:{[name:string]:HTMLElement};

    // parses the TICK script to see if there are any HTTP endpoints to watch and adds them here if need be
    private updateHttpEndpoints()
    {
        // first parse the tick script to find all endpoints
        // note that we parse the script returned by indlux (which is nicely formatted, so our regex can be simple) instead
        // of the user script in the ediotr that could have arbitrary spacing, comments, etc
        const regex = /\n\s*\|httpOut\('([^']*)'\)/gm;
        let script = this.task.script;
        let endpoints:string[] = [];
        let match:RegExpExecArray;
        while(match = regex.exec(script))
        {
            let endpoint = match[1];
            if(endpoint && !(endpoint in endpoints))
                endpoints.push(endpoint);
        }

        // remove old endpoint HTML elements
        this.httpEndpointColumns = {};
        Utils.clearChildren(this.httpEndpointsContainer);

        // create a row for each in the table
        for(let endpoint of endpoints)
        {
            let header = document.createElement("h4");
            header.innerText = endpoint;
            header.style.textDecoration = "underline";
            header.classList.add("endpointDataHeader");
            this.httpEndpointsContainer.appendChild(header);

            let body = document.createElement("div");
            body.classList.add('endpointDataDiv');
            this.httpEndpointColumns[endpoint] = body;
            this.httpEndpointsContainer.appendChild(body);
        }
    }

    // error handler called when a http endpoint request fails. we don't want to bother the user with a big modal dialog, so just put
    // the error message straight into the text
    private static endpointErrorHandler(xhr:JQueryXHR, elem:HTMLElement)
    {
        let errorString:string;
        try { errorString = xhr.responseJSON.error; }
        catch(_) { errorString = xhr.statusText || "Unknown error"; }

        let span = document.createElement("span");
        span.style.color = "red";
        span.innerText = errorString;

        Utils.clearChildren(elem);
        elem.appendChild(span);
    }

    private static endpointDataHandler(data:Influx.QueryResult, elem:HTMLElement)
    {
        Utils.clearChildren(elem);
        if(data.series)
        {
            let table = EditorPage.formatEndpointData(data.series);
            elem.appendChild(table);
        }
        else
        {
            let span = document.createElement("span");
            span.innerText = "(no data)";
            elem.appendChild(span);
        }
    }

    private static formatEndpointData(allSeries:Influx.Series[]):Node
    {
        // this is a bad algorithm, but there's so little data (hopefully) it won't matter
        let maps:{[key:string]:string}[] = [];
        let columnNames:{[key:string]:boolean} = {};
        let tagNames:{[key:string]:boolean} = {};

        // flatten out columns and tags, and get the names of all columns and tags
        for(let series of allSeries)
        {
            let map:{[key:string]:string} = {};

            if(series.tags)
                for(let name in series.tags)
                    if(series.tags.hasOwnProperty(name))
                    {
                        let value = series.tags[name];
                        tagNames[name] = true;
                        map[name] = value;
                    }

            for(let i = 0; i < series.columns.length; i++)
            {
                let name = series.columns[i];
                let value = String(series.values[0][i]);
                columnNames[name] = true;
                map[name] = value;
            }

            maps.push(map);
        }

        // sort tag and column names to get list of all columns we want
        let makeSortedArray:(g:{[key:string]:boolean})=>string[] = g =>
        {
            let res:string[] = [];
            for(let n in g)
                if(g.hasOwnProperty(n))
                    res.push(n);
            res.sort();
            return res;
        };


        // create the column headers
        let allNames = makeSortedArray(tagNames).concat(makeSortedArray(columnNames));
        let thead = document.createElement("thead");
        let thr = document.createElement("tr");
        for(let name of allNames)
        {
            let th = document.createElement("th");
            th.appendChild(document.createTextNode(name));
            thr.appendChild(th);
        }
        thead.appendChild(thr);

        // create the body
        let tbody = document.createElement("tbody");
        for(let map of maps)
        {
            let tr = document.createElement("tr");
            for(let name of allNames)
            {
                let td = document.createElement("td");
                td.classList.add("endpointTd");
                if(name in map)
                    td.appendChild(document.createTextNode(map[name]));
                tr.appendChild(td);
            }
            tbody.appendChild(tr);
        }

        // create the table itself
        let table = document.createElement("table");
        table.classList.add("table");
        table.appendChild(thead);
        table.appendChild(tbody);
        return table;
    }

    //==================================================================================================================
    // ERROR TOOLBOX
    //==================================================================================================================

    private errorsToolboxButtonIcon:HTMLElement;
    private errorsToolboxButtonText:HTMLElement;
    private errorsTextContainer:HTMLElement;
    private errorsNotificationText:HTMLElement;

    private setRuntimeErrors(error:string)
    {
        if(!error)
        {
            this.errorsTextContainer.innerText = "";
            this.errorsToolboxButtonIcon.style.removeProperty("color");
            this.errorsToolboxButtonText.style.removeProperty("color");
            this.errorsNotificationText.innerText = "";
        }
        else
        {
            this.errorsTextContainer.innerText = error;
            this.errorsToolboxButtonIcon.style.color = "#ffa500";
            this.errorsToolboxButtonText.style.color = "#ffa500";
            this.errorsNotificationText.innerText = "Runtime errors running script. Check the errors tab.";
        }
    }

    //==================================================================================================================
    // ERROR HANDLING
    //==================================================================================================================

    // primary error handler. shows a modal dialog.
    private static mainErrorHandler(url:string, xhr:JQueryXHR)
    {
        let errorString:string = null;
        try  { errorString = xhr.responseJSON.error; } catch(_) { } // if we can't parse result as json, we'll use default handler
        if(!errorString || !/^invalid TICKscript:/.test(errorString))
            Utils.defaultApiErrorHandler(url, xhr);
        else
            EditorPage.handleScriptError(errorString.substr("invalid TICKscript: ".length));
    }

    // try to parse a kapacitor error message (they're kind of light on details). shows a modal dialog.
    private static handleScriptError(error:string)
    {
        let match = /^invalid TICKscript:[^|]*\\| line (\d+)/.exec(error);
        let line = match ? Number(match[1]) : 0;
        let dialogErrorTitle = document.getElementById("dialogErrorTitle");
        dialogErrorTitle.innerText = "Invalid TICK Script";
        let dialogErrorBody = document.getElementById("dialogErrorBody");
        dialogErrorBody.innerHTML = "";
        if(line > 0)
        {
            let lineNoText = document.createElement("p");
            lineNoText.innerText = "Line number: " + line;
            let errorStrText = document.createElement("p");
            errorStrText.innerText = error;
            dialogErrorBody.appendChild(lineNoText);
            dialogErrorBody.appendChild(errorStrText);
        }
        else
        {
            dialogErrorBody.innerText = error;
        }
        Utils.modal($("#dialogError"), "show");
    }

    //==================================================================================================================
    // TASK ENABLED/DISABLED
    //==================================================================================================================

    private enableButton:HTMLButtonElement;
    private dialogUnsavedChanges:HTMLElement;

    // sets up the button which shows enabled and disabled
    private setEnabledStyle()
    {
        if(this.task.status === "enabled")
        {
            this.enableButton.innerText = "Disable";
            this.enableButton.className = "btn btn-danger";
        }
        else
        {
            this.enableButton.innerText = "Enable";
            this.enableButton.className = "btn btn-success";
        }
    }

    // method called by the HTML page when clicking the "enable" or "disable" button.
    // if the user clicks "enable"/"disable" with unsaved changes, we also need to ask if they want to save the script
    public doToggleEnabled()
    {
        let script = this.editor.getValue();
        if(this.lastSavedScript !== script)
            Utils.modal($(this.dialogUnsavedChanges), "show");
        else
            this.realToggleEnabled(false, false);
    }

    // actually does the enable/disable toggle (called by HTML page sometimes)
    public realToggleEnabled(saveScript:boolean, dismissDialog:boolean)
    {
        let currentlyEnabled = this.task.status === "enabled";
        let changes:Kapacitor.Task = { status: currentlyEnabled ? "disabled" : "enabled" };
        if(saveScript)
            changes.script = this.editor.getValue();
        if(dismissDialog)
            Utils.modal($(this.dialogUnsavedChanges), "hide");
        this.patch(changes);
    }

    //==================================================================================================================

    private initEditor()
    {
        TickMode.defineMode();
        this.editor = ace.edit("editor");
        this.editor.session.setMode("ace/mode/tick");
        this.editor.setTheme("ace/theme/tomorrow_night");
        this.editor.setOptions({ enableBasicAutocompletion:true });
        this.editor.$blockScrolling = Infinity;
        let completer = new TickCompleter();
        (<any>this.editor).completers = [completer];
        completer.addLiveAutocomplete(this.editor);
    }

    public init()
    {
        // html elements
        this.enableButton = <HTMLButtonElement>document.getElementById("enableButton");
        this.dialogUnsavedChanges = document.getElementById("dialogUnsavedChanges");
        this.layoutLeft = document.getElementById("layoutLeft");
        this.layoutRight = document.getElementById("layoutRight");
        this.debugToolboxPanel = document.getElementById("debugToolboxPanel");
        this.errorsToolboxPanel = document.getElementById("errorsToolboxPanel");
        this.statsTextBlock = document.getElementById("statsTextBlock");
        this.httpEndpointsContainer = document.getElementById("httpEndpointsContainer");
        this.showMeasurementsLink = <HTMLAnchorElement>document.getElementById("showMeasurementsLink");
        this.errorsToolboxButtonIcon = document.getElementById("errorsToolboxButtonIcon");
        this.errorsToolboxButtonText = document.getElementById("errorsToolboxButtonText");
        this.errorsTextContainer = document.getElementById("errorsTextContainer");
        this.errorsNotificationText = document.getElementById("errorsNotificationText");

        // get ID from query string
        let id = Utils.queryString("taskId");
        let name = Utils.idToTaskName(id);
        document.getElementById("nameHeader").innerText = name;
        document.title = name;

        // setup text editor
        this.initEditor();

        // load script from Kapacitor
        Kapacitor.api("tasks/" + id, t => this.updateTask(t, true), EditorPage.mainErrorHandler);
    }
}
