/// <reference types="jquery" />
/// <reference path="utils.ts" />
/// <reference path="kapacitor.ts" />

import errorDialogText = Utils.errorDialogText;
enum TaskListSortColumn { Name, Created, Database, Executing }
class TaskRow extends HTMLTableRowElement { task:Kapacitor.Task; }

class IndexPage
{
    private loadedInitialData:boolean;
    private tableBody:HTMLTableSectionElement;
    private editButtonsTemplate:HTMLElement;

    private initTemplates()
    {
        this.editButtonsTemplate.remove();
        this.editButtonsTemplate.id = null;
        this.editButtonsTemplate.hidden = false;
    }

    private static linkButton(parent:Node, buttonClass:string, func:(link:HTMLAnchorElement)=>void)
    {
        let selector = "." + buttonClass;
        let node = $(parent).children(selector);
        func(<HTMLAnchorElement>node.get(0));
        (<any>node).tooltip();
    }

    private addTaskRow(task:Kapacitor.Task, isNew:boolean)
    {
        let row = <TaskRow>document.createElement("tr");
        row.task = task;
        row.id = "task_" + task.id;
        if(isNew) row.style.backgroundColor = "green";

        // task name
        let name = Utils.idToTaskName(task.id);
        Utils.textColumn(row, name);
        this.allTaskNames[name] = true;

        // database
        let database = task.dbrps[0].db;
        Utils.textColumn(row, database);

        // created date
        let createdFormat = new Date(task.created).toLocaleString();
        Utils.textColumn(row, createdFormat);

        // status
        let status = document.createElement("span");
        status.style.fontWeight = "bold";
        if(task.executing)
        {
            status.style.color = "#5cb85c";
            status.innerText = "Enabled";
        }
        else if(task.status === "enabled")
        {
            status.style.color = "#ffa500";
            status.innerText = "Enabled Not Executing";
        }
        else
        {
            status.style.color = "#d9534f";
            status.innerText = "Disabled";
        }
        Utils.column(row, status);

        // edit buttons
        let buttonGroup = this.editButtonsTemplate.cloneNode(true);
        IndexPage.linkButton(buttonGroup, "task-openEditor", a => a.href = "editor.html?taskId=" + task.id);
        IndexPage.linkButton(buttonGroup, "task-duplicate", a => { a.href = "javascript:void(0)"; a.onclick = () => this.cloneTask(task); });
        IndexPage.linkButton(buttonGroup, "task-rename", a => { a.href = "javascript:void(0)"; a.onclick = () => this.renameTask(task); });
        IndexPage.linkButton(buttonGroup, "task-delete", a => { a.href = "javascript:void(0)"; a.onclick = () => this.showDeleteDialog(task.id); });
        Utils.column(row, buttonGroup);

        $(this.tableBody).append(row);
        if(isNew)
        {
            $("html, body").animate({ scrollTop: $(row).offset().top }, 1000); // scroll element into focus
        }
    }

    private removeTaskRow(taskId:string)
    {
        let row = document.getElementById("task_" + taskId);
        if(row)
        {
            row.parentElement.removeChild(row);
            let name = Utils.idToTaskName(taskId);
            if(name in this.allTaskNames)
                delete this.allTaskNames[name];
        }
    }

    private generateTable(tasks:Kapacitor.TaskList)
    {
        this.loadedInitialData = true;
        tasks.tasks.forEach(task => this.addTaskRow(task, false));
        this.sortBy(TaskListSortColumn.Name);
    }

    //==================================================================================================================
    // SORTING
    //==================================================================================================================

    private currentSortColumn:TaskListSortColumn;
    private nameColummHeader:HTMLElement;
    private createdColumnHeader:HTMLElement;
    private executingColumnHeader:HTMLElement;
    private databaseColumnHeader:HTMLElement;

    private static compare(x:Kapacitor.Task, y:Kapacitor.Task, sortBy:TaskListSortColumn):number
    {
        switch(sortBy)
        {
            case TaskListSortColumn.Name:
                let xName = Utils.idToTaskName(x.id).toLowerCase(), yName = Utils.idToTaskName(y.id).toLowerCase();
                return xName > yName ? 1 : xName < yName ? -1 : 0;
            case TaskListSortColumn.Created:
                let xCreated = new Date(x.created).getTime(), yCreated = new Date(y.created).getTime();
                return xCreated > yCreated ? 1 : xCreated < yCreated ? -1 : 0;
            case TaskListSortColumn.Database:
                // if they have the same database, compare by name
                let xdb = x.dbrps[0].db.toLowerCase(), ydb = y.dbrps[0].db.toLowerCase();
                return xdb > ydb ? 1 : xdb < ydb ? -1 : IndexPage.compare(x, y, TaskListSortColumn.Name);
            case TaskListSortColumn.Executing:
                // if they have the same status, compare by name
                let res = (x.executing ? 0 : 1) - (y.executing ? 0 : 1);
                return res == 0 ? IndexPage.compare(x, y, TaskListSortColumn.Name) : res;
            default:
                throw "Invalid sort column";
        }
    }

    public sortBy(column:TaskListSortColumn)
    {
        if(!this.loadedInitialData) return;
        if(column == this.currentSortColumn) return;
        this.currentSortColumn = column;
        this.nameColummHeader.innerHTML = "Name" + (column == TaskListSortColumn.Name ? " &#x25BC;" : "");
        this.createdColumnHeader.innerHTML = "Created" + (column == TaskListSortColumn.Created ? " &#x25BC;" : "");
        this.databaseColumnHeader.innerHTML = "Database" + (column == TaskListSortColumn.Database ? " &#x25BC;" : "");
        this.executingColumnHeader.innerHTML = "Status" + (column == TaskListSortColumn.Executing ? " &#x25BC;" : "");
        let hrows = this.tableBody.rows, rows:TaskRow[] = [];
        for(let i = 0; i < hrows.length; i++)
            rows.push(<TaskRow>hrows.item(i));
        rows.sort((x, y) => IndexPage.compare(x.task, y.task, column));
        while(this.tableBody.lastChild)
            (<TaskRow>this.tableBody.lastChild).remove();
        for(let row of rows)
            this.tableBody.appendChild(row);
    }

    //==================================================================================================================
    // NEW TASK DIALOG
    //==================================================================================================================

    private allTaskNames:{[name:string]:boolean} = {};
    private dialogCreate:HTMLDivElement;
    private dialogCreateName:HTMLInputElement;
    private dialogCreateButton:HTMLButtonElement;
    private dialogCreateDb:HTMLSelectElement;
    private dialogCreateRp:HTMLSelectElement;
    private dialogCreateHeader:HTMLHeadingElement;
    private dbrps:{[name:string]:string[]};

    private static DEFAULT_TICK_SCRIPT =
`var example = stream
    |from()
        .database('__DB__')
        .retentionPolicy('__RP__')
        .measurement('disk_free_percent_rootfs')
        .groupBy('host', 'cluster')
`;

    private static dbrpCompareHack(a:string):string
    {
        return a === "default"  ? "__0000" :
               a === "graphite" ? "__0001" :
               a === "telegraf" ? "__0002" : a;
    }

    private static dbrpCompare(a:string, b:string):number
    {
        a = IndexPage.dbrpCompareHack(a).toLowerCase();
        b = IndexPage.dbrpCompareHack(b).toLowerCase();
        return a.localeCompare(b);
    }

    private static parseCsvColumn(csv:string, column:number):string[]
    {
        try
        {
            let results = Utils.parseCsv(csv);
            let isFirst = true;
            let values:string[] = [];
            for(let row of results)
            {
                if(isFirst)
                {
                    isFirst = false;
                    continue;
                }
                if(row.length >= column + 1)
                {
                    let value = row[column];
                    if(value && value.length)
                        values.push(value);
                }
            }
            values.sort(IndexPage.dbrpCompare);
            return values;
        }
        catch(ex)
        {
            console.log(ex);
            errorDialogText("Error parsing CSV", "Error parsing database or retention policy CSV:\n" + ex.toString());
        }
    }

    private loadDbs(values:string[])
    {
        for(let db of values)
        {
            if(db === '_internal' || db === '_kapacitor' || db === 'chronograf' || db === 'alertBuilder')
                continue;
            let opt = document.createElement("option");
            opt.text = db; opt.value = db;
            this.dialogCreateDb.options.add(opt);
            this.dbrps = {};
            Influx.queryCsv("SHOW RETENTION POLICIES ON " + db,
                csv => this.dbrps[db] = IndexPage.parseCsvColumn(csv, 2));
        }
    }

    private static selectOptionOrDefault(selector:HTMLSelectElement, value:string)
    {
        if(value)
        {
            for(let i = 0; i < selector.options.length; i++)
            {
                let optionValue:string = (<any>selector.options[i]).value;
                if(optionValue === value)
                {
                    selector.selectedIndex = i;
                    return;
                }
            }
            console.log("Warning: could not find default value " + value + " in select combo box");
        }
        selector.selectedIndex = 0;
    }

    private resetRps(clone:Kapacitor.Task)
    {
        IndexPage.selectOptionOrDefault(this.dialogCreateDb, clone ? clone.dbrps[0].db : null);
        this.updateRps();
        IndexPage.selectOptionOrDefault(this.dialogCreateRp, clone ? clone.dbrps[0].rp : null);
    }

    private updateRps()
    {
        while(this.dialogCreateRp.options.length > 0)
            this.dialogCreateRp.options.remove(0);
        let db = this.dialogCreateDb.value;
        if(db in this.dbrps && this.dbrps[db].length)
        {
            for(let rp of this.dbrps[db])
            {
                let opt = document.createElement("option");
                opt.text = rp; opt.value = rp;
                this.dialogCreateRp.options.add(opt);
            }
        }
        else
        {
            let opt = document.createElement("option");
            opt.text = "default"; opt.value = "default";
            this.dialogCreateRp.options.add(opt);
        }
    }

    private isValidTaskName(name:string)
    {
        name = name.trim();
        return name.length > 0 && name.length < 100 && /^[-._ a-zA-Z0-9]*$/.test(name) && !(name in this.allTaskNames);
    }

    public createEmptyTask() { this.showCreateDialog(null, false); }
    private cloneTask(task:Kapacitor.Task) { this.showCreateDialog(task, false) }
    private renameTask(task:Kapacitor.Task) { this.showCreateDialog(task, true); }
    private showCreateDialog(clone:Kapacitor.Task, isRename:boolean)
    {
        if(!this.dbrps)
        {
            Utils.errorDialogText("Error loading databases", "Haven't loaded databases/retention policies yet. Check browser logs");
            return;
        }

        if(clone)
        {
            this.dialogCreateName.value = Utils.idToTaskName(clone.id);
            $(this.dialogCreateButton).prop('disabled', true);
        }
        else
        {
            this.dialogCreateName.value = "";
            $(this.dialogCreateButton).prop('disabled', true);
        }

        if(isRename)
        {
            this.dialogCreateHeader.innerText = "Rename " + Utils.idToTaskName(clone.id);
            this.dialogCreateButton.innerText = "Rename";
        }
        else
        {
            this.dialogCreateHeader.innerText = "Create New Task";
            this.dialogCreateButton.innerText = "Create";
        }


        this.dialogCreateName.oninput = () => this.createDialogOnInput();
        this.dialogCreateName.onkeyup = evt => this.createDialogKeyUp(evt);
        this.dialogCreateButton.onclick = () => this.createDialogConfirm(clone, isRename);
        this.resetRps(clone);
        Utils.modal($(this.dialogCreate), "show");
        this.dialogCreateName.select();
    }

    private createDialogOnInput()
    {
        let name = this.dialogCreateName.value;
        $(this.dialogCreateButton).prop('disabled', !this.isValidTaskName(name));
    }

    private createDialogKeyUp(evt:Event)
    {
        evt = evt || window.event;
        evt.preventDefault();
        if((<any>evt).keyCode == 13)
        {
            this.dialogCreateButton.click();
            return false;
        }
        return true;
    }

    private createDialogConfirm(clone:Kapacitor.Task, isRename:boolean)
    {
        let name = this.dialogCreateName.value.trim().replace(/\s/g, "_");
        let db = this.dialogCreateDb.value;
        let rp = this.dialogCreateRp.value;
        if(this.isValidTaskName(name))
        {
            if(isRename)
                this.doRename(clone, name, db, rp);
            else
                this.doCreate(clone, name, db, rp);
            Utils.modal($(this.dialogCreate), "hide");
        }
    }

    private doCreate(clone:Kapacitor.Task, name:string, db:string, rp:string)
    {
        let newScript = IndexPage.DEFAULT_TICK_SCRIPT.replace(/__DB__/g, db).replace(/__RP__/g, rp).replace(/__TASK_NAME__/g, name);
        let task:Kapacitor.Task =
            {
                id:name,
                type:"stream",
                dbrps:[{db:db,rp:rp}],
                script:clone?clone.script:newScript,
                vars:clone?clone.vars:null,
            };
        Kapacitor.api("tasks", t2 => this.addTaskRow(t2, true), null, "POST", task);
    }

    private doRename(task:Kapacitor.Task, name:string, db:string, rp:string)
    {
        let oldId = task.id;
        let patched:Kapacitor.Task = { id:name, dbrps:[{db:db,rp:rp}] };
        Kapacitor.api("tasks/" + task.id, t => this.renameComplete(oldId, t), null, "PATCH", patched)
    }

    private renameComplete(oldId:string, newTask:Kapacitor.Task)
    {
        this.removeTaskRow(oldId);
        this.addTaskRow(newTask, true);
    }

    //==================================================================================================================
    // DELETE TASK DIALOG
    //==================================================================================================================

    private dialogDelete:HTMLDivElement;
    private dialogDeleteText:HTMLSpanElement;
    private dialogDeleteButton:HTMLButtonElement;

    private showDeleteDialog(taskId:string)
    {
        this.dialogDeleteText.innerHTML = "Are you sure you want to PERMANENTLY delete task " + taskId + "? This cannot be undone!";
        this.dialogDeleteButton.onclick = () => this.deleteDialogConfirm(taskId);
        Utils.modal($(this.dialogDelete), "show");
    }

    private deleteDialogConfirm(taskId:string)
    {
        Kapacitor.api("tasks/" + taskId, () => this.removeTaskRow(taskId), null, "DELETE");
        Utils.modal($(this.dialogDelete), "hide");
    }

    //==================================================================================================================

    public init()
    {
        this.dialogCreate = <HTMLDivElement>document.getElementById("dialogCreate");
        this.dialogCreateName = <HTMLInputElement>document.getElementById("dialogCreateName");
        this.dialogCreateDb = <HTMLSelectElement>document.getElementById("dialogCreateDb");
        this.dialogCreateRp = <HTMLSelectElement>document.getElementById("dialogCreateRp");
        this.dialogCreateButton = <HTMLButtonElement>document.getElementById("dialogCreateButton");
        this.dialogCreateHeader = <HTMLHeadingElement>document.getElementById("dialogCreateHeader");
        this.dialogDelete = <HTMLDivElement>document.getElementById("dialogDelete");
        this.dialogDeleteText = <HTMLSpanElement>document.getElementById("dialogDeleteText");
        this.dialogDeleteButton = <HTMLButtonElement>document.getElementById("dialogDeleteButton");
        this.nameColummHeader = document.getElementById("nameColumnHeader");
        this.createdColumnHeader = document.getElementById("createdColumnHeader");
        this.databaseColumnHeader = document.getElementById("databaseColumnHeader");
        this.executingColumnHeader = document.getElementById("executingColumnHeader");
        this.tableBody = <HTMLTableSectionElement>document.getElementById("tasks-tbody");
        this.editButtonsTemplate = document.getElementById("editButtonsTemplate");

        this.initTemplates();
        Kapacitor.api("tasks", tasks => this.generateTable(tasks));
        Influx.queryCsv("SHOW DATABASES", csv => this.loadDbs(IndexPage.parseCsvColumn(csv, 2)));
    }
}

