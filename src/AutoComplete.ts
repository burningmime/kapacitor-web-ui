/// <reference types="ace" />
/// <reference path="./TickScript.ts" />

class TickCompletion
{
    caption:string;
    snippet:string;
    meta:string;
    score:number;
    doc:{docHTML:string};

    constructor(snippet:string, doc?:string, caption?:string)
    {
        this.caption = caption || snippet;
        this.snippet = snippet;
        this.meta = "";
        this.score = 0;
        this.doc = doc ? {docHTML:TickCompletion.htmlizeDocComment(doc)} : null;
    }

    private static htmlizeDocComment(doc:string):string
    {
        let parts = doc.split("\n");
        let html;
        if(parts.length > 1)
        {
            let comment = "";
            for(let i = 0; i < parts.length - 1; i++)
            {
                if(i != 0) comment += '<br/><br/>';
                comment += Utils.escapeHtml(parts[i]);
            }
            let usage = Utils.escapeHtml(parts[parts.length - 1]);
            html = '<p style="text-wrap:normal;white-space:normal">' + comment + '</p>' +
                '<pre style="white-space:normal;word-wrap:break-word">' + usage + '</pre>';
        }
        else
        {
            html = '<p style="text-wrap:normal;white-space:normal">' + doc + '</p>';
        }
        return '<div style="width:450px;height:auto">' + html + '</div>';
    }
}

// these are needed because the typings.d for Ace is woefully incomplete
interface TokenExt extends AceAjax.TokenInfo { type:string; }

class TickCompleter
{
    private methodCompletions:TickCompletion[];
    private propertyCompletions:{[name:string]:TickCompletion[]};
    private static TokenIterator:any;
    private delayOpenPopup:boolean;

    constructor()
    {
        // by default don't call "require" since this class needs to be required in scope without ace available
        if(!TickCompleter.TokenIterator)
            TickCompleter.TokenIterator = ace.require("ace/token_iterator").TokenIterator;
        let lang = TickScript.initTickLanguage();
        let methodCompletions:TickCompletion[] = [];
        let propertyCompletions:{[name:string]:TickCompletion[]} = {};
        for(let name in lang.allMethods)
        {
            if(lang.allMethods.hasOwnProperty(name))
            {
                let method = lang.allMethods[name];
                methodCompletions.push(new TickCompletion(name, method.comment));
                let pcs:TickCompletion[] = [];
                let mps = method.resultType.properties;
                for(let pn in mps)
                    if(mps.hasOwnProperty(pn))
                        pcs.push(new TickCompletion(pn, mps[pn].comment));
                pcs.sort((x, y) => x.caption.toLowerCase().localeCompare(y.caption.toLowerCase()));
                for(let i = 0; i < pcs.length; i++) pcs[i].score = 1000 - i;
                propertyCompletions[name] = pcs;
            }
        }
        methodCompletions.sort((x, y) => x.caption.toLowerCase().localeCompare(y.caption.toLowerCase()));
        for(let i = 0; i < methodCompletions.length; i++) methodCompletions[i].score = 1000 - i;
        this.methodCompletions = methodCompletions;
        this.propertyCompletions = propertyCompletions;
    }

    public getCompletions(state:string, session:AceAjax.IEditSession, pos:AceAjax.Position, prefix:string,
        callback:(_:any, completions:TickCompletion[])=>void)
    {
        let completions:TickCompletion[] = [];
        let token = <TokenExt>session.getTokenAt(pos.row, pos.column);
        if(!token || !token.type) return;
        // the tokenizer returns these as ".function" and ".tag" since tehre are predefined highlight rules for them
        if(/\.function$/.test(token.type))
            completions = this.completeMethod();
        else if(/\.tag$/.test(token.type))
            completions = this.completeProperty(session, pos);
        else
            return;
        callback(null, completions);
    }

    private completeMethod():TickCompletion[]
    {
        // TODO only allow the right methods to chain
        return this.methodCompletions;
    }

    private completeProperty(session:AceAjax.IEditSession, pos:AceAjax.Position):TickCompletion[]
    {
        let methodName = TickCompleter.findMethod(session, pos);
        if(methodName && methodName.length > 0 && methodName in this.propertyCompletions && this.propertyCompletions.hasOwnProperty(methodName))
            return this.propertyCompletions[methodName];
        return [];
    }

    private static findMethod(session:AceAjax.IEditSession, pos:AceAjax.Position):string
    {
        let iterator = new TickCompleter.TokenIterator(session, pos.row, pos.column);
        let token = <TokenExt>iterator.getCurrentToken();
        while(token)
        {
            if(TickCompleter.isToken(token, ".function"))
                return token.value;
            token = <TokenExt>iterator.stepBackward();
        }
        return null;
    }

    private static isToken(token:TokenExt, substr:string)
    {
        return token.type.lastIndexOf(substr) >= 0;
    }

    public getDocTooltip(selected:TickCompletion)
    {
        return selected && selected.doc ? selected.doc : null;
    }

    public addLiveAutocomplete(e1:AceAjax.Editor)
    {
        // all this is a workaround ebcause I couldn't get the "live autocomplete" feature of ACE to work at all

        // when the user is about to insert a . or a |, mark that we need to open a popup eventually
        e1.on('change', evt =>
        {
            if(evt.action === 'insert' && evt.lines && evt.lines.length == 1 && (evt.lines[0] === '.' || evt.lines[0] === '|'))
                this.delayOpenPopup = true;
        });

        // after the user has executed a command, if we have a popup to open, actually open it
        (<any>e1.commands).on('afterExec', (evt:any) =>
        {
            if(this.delayOpenPopup)
            {
                this.delayOpenPopup = false;
                let editor = evt.editor;
                let isActive = editor.completer && editor.completer.activated;
                if(!isActive)
                {
                    if(!editor.completer)
                    {
                        let Autocomplete = ace.require("ace/autocomplete").Autocomplete;
                        editor.completer = new Autocomplete();
                    }
                    editor.completer.autoInsert = false;
                    editor.completer.showPopup(editor);
                }
            }
        });
    }
}
