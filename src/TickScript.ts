/// <reference types="ace" />

//==================================================================================================================
// TYPES
//==================================================================================================================

namespace TickScript
{
    export interface TickLanguage
    {
        allMethods:TickMethods;
        defaultMethods:TickMethods;
        rootMethods:TickMethods;
        batchMethods:TickMethods;
        streamMethods:TickMethods;
    }

    export interface TickProperties { [name:string]:TickProperty }
    export class TickProperty
    {
        comment:string;
        node:TickNode;

        constructor(comment:string, node:TickNode)
        {
            this.comment = comment;
            this.node = node;
        }
    }

    export interface TickMethods { [name:string]:TickMethod }
    export class TickMethod
    {
        comment:string;
        resultType:TickNode;

        constructor(resultType:TickNode, comment:string)
        {
            if(!resultType)
                throw "Tick method has no result type!";
            this.comment = comment;
            this.resultType = resultType;
        }
    }

    export interface TickNodes { [name:string]:TickNode }
    export class TickNode
    {
        name:string;
        properties:TickProperties;
        chainedMethods:TickMethods;

        constructor(name:string, chainedMethods:TickMethods, properties:{[propertyName:string]:string})
        {
            this.name = name;
            this.chainedMethods = chainedMethods;
            this.properties = {};
            for(let propName in properties)
                if(properties.hasOwnProperty(propName))
                    this.properties[propName] = new TickProperty(properties[propName], this);
        }
    }

    export function initTickLanguage():TickLanguage
    {
        //==================================================================================================================
        // NODES
        //==================================================================================================================

        // need to fill these once we have nodes
        let defaultMethods:TickMethods = {};
        let batchMethods:TickMethods = {};
        let streamMethods:TickMethods = {};
        let influxOutMethods:TickMethods = {};
        let rootMethods:TickMethods = {};

        let nodeList:TickNode[] = [
            new TickNode('BatchNode', batchMethods, {}),
            new TickNode('StreamNode', streamMethods, {}),
            new TickNode('AlertNode', defaultMethods, {
                all: 'Indicates an alert should trigger only if all points in a batch match the criteria. Does not apply to stream alerts.\nnode.all()',
                crit: 'Filter expression for the CRITICAL alert level. An empty value indicates the level is invalid and is skipped.\nnode.crit(value ast.LambdaNode)',
                critReset: 'Filter expression for reseting the CRITICAL alert level to lower level.\nnode.critReset(value ast.LambdaNode)',
                details: 'You probably should just use message instead, but see docs if you need this.\nnode.details(value string)',
                durationField: 'Optional field key to add the alert duration to the data. The duration is always in units of nanoseconds.\nnode.durationField(value string)',
                exec: 'Execute a command whenever an alert is triggered and pass the alert data over STDIN in JSON format.\nnode.exec(executable string, args ...string)',
                flapping: 'Perform flap detection on the alerts. The method used is similar method to Nagios. See docs for more info.\nnode.flapping(low float64, high float64)',
                history: 'Number of previous states to remember when computing flapping levels and checking for state changes. Minimum value is 2 in order to keep track of current and previous states. Default: 21\nnode.history(value int64)',
                id: 'Template for constructing a unique ID for a given alert. See docs for details.\nnode.id(value string)',
                idField: 'Optional field key to add to the data, containing the alert ID as a string.\nnode.idField(value string)',
                idTag: 'Optional tag key to use when tagging the data with the alert ID.\nnode.idTag(value string)',
                info: 'Filter expression for the INFO alert level. An empty value indicates the level is invalid and is skipped.\nnode.info(value ast.LambdaNode)',
                infoReset: 'Filter expression for reseting the INFO alert level to lower level.\nnode.infoReset(value ast.LambdaNode)',
                levelField: 'Optional field key to add to the data, containing the alert level as a string.\nnode.levelField(value string)',
                levelTag: 'Optional tag key to use when tagging the data with the alert level.\nnode.levelTag(value string)',
                log: 'Log JSON alert data to file. One event per line. Must specify the absolute path to the log file. It will be created if it does not exist.\nnode.log(filepath string)',
                mode: 'File\'s mode and permissions, default is 0600 NOTE: The leading 0 is required to interpret the value as an octal integer.\nnode.log(filepath string).mode(value int64)',
                message: 'Template for constructing a meaningful message for the alert. See documentation.\nnode.message(value string)',
                messageField: 'Optional field key to add to the data, containing the alert message.\nnode.messageField(value string)',
                noRecoveries: 'Do not send recovery alerts.\nnode.noRecoveries()',
                pagerDuty: 'Send the alert to OpenDuty! Routing is done based on the message field\nnode.pagerDuty()',
                post: 'HTTP POST JSON alert data to a specified URL.\nnode.post(url string)',
                stateChangesOnly: 'Only sends events where the state changed. You probably should be using this. See docs for why.\nnode.stateChangesOnly(maxInterval ...time.Duration)',
                warn: 'Filter expression for the WARNING alert level. An empty value indicates the level is invalid and is skipped.\nnode.warn(value ast.LambdaNode)',
                warnReset: 'Filter expression for reseting the WARNING alert level to lower level.\nnode.warnReset(value ast.LambdaNode)', }),
            new TickNode('CombineNode', defaultMethods, {
                as: 'Prefix names for all fields from the respective nodes. Each field from the parent nodes will be prefixed with the provided name and a \'.\'. See examples in docs.\nnode.as(names ...string)',
                delimiter: 'The delimiter between the As names and existing field an tag keys. Can be the empty string, but you are responsible for ensuring conflicts are not possible if you use the empty string.\nnode.delimiter(value string)',
                max: 'Maximum number of possible combinations. Since the number of possible combinations can grow very rapidly you can set a maximum number of combinations allowed. If the max is crossed, an error is logged and the combinations are not calculated. Default: 10,000\nnode.max(value int64)',
                tolerance: 'The maximum duration of time that two incoming points can be apart and still be considered to be equal in time. The joined data point\'s time will be rounded to the nearest multiple of the tolerance duration.\nnode.tolerance(value time.Duration)', }),
            new TickNode('DefaultNode', defaultMethods, {
                field: 'Define a field default.\nnode.field(name string, value interface{})',
                tag: 'Define a tag default.\nnode.tag(name string, value string)', }),
            new TickNode('DeleteNode', defaultMethods, {
                field: 'Delete a field.\nnode.field(name string, value interface{})',
                tag: 'Delete a tag.\nnode.tag(name string, value string)', }),
            new TickNode('DerivativeNode', defaultMethods, {
                as: 'The new name of the derivative field. Default is the name of the field used when calculating the derivative.\nnode.as(value string)',
                nonNegative: 'If called the derivative will skip negative results.\nnode.nonNegative()',
                unit: 'The time unit of the resulting derivative value. Default: 1s\nnode.unit(value time.Duration)', }),
            new TickNode('EvalNode', defaultMethods, {
                as: 'List of names for each expression. The expressions are evaluated in order. The result of an expression may be referenced by later expressions via the name provided.\nnode.as(names ...string)',
                keep: 'If called the existing fields will be preserved in addition to the new fields being set. See dcos for why this is important.\nnode.keep(fields ...string)',
                quiet: 'Suppress errors during evaluation.\nnode.quiet()',
                tags: 'Convert the result of an expression into a tag. The result must be a string. Use the string() expression function to convert types.\nnode.tags(names ...string)', }),
            new TickNode('FlattenNode', defaultMethods, {
                delimiter: 'The delimiter between field name parts\nnode.delimiter(value string)',
                on: 'Specify the dimensions on which to flatten the points.\nnode.on(dims ...string)',
                tolerance: 'The maximum duration of time that two incoming points can be apart and still be considered to be equal in time. The joined data point\'s time will be rounded to the nearest multiple of the tolerance duration.\nnode.tolerance(value time.Duration)', }),
            new TickNode('FromNode', defaultMethods, {
                database: 'The database name. If empty any database will be used.\nnode.database(value string)',
                groupBy: 'Group the data by a set of tags. Can pass literal * to group by all dimensions.\nnode.groupBy(tag ...interface{})',
                groupByMeasurement: 'If set will include the measurement name in the group ID. Along with any other group by dimensions.\nnode.groupByMeasurement()',
                measurement: 'The measurement name If empty any measurement will be used.\nnode.measurement(value string)',
                retentionPolicy: 'The retention policy name If empty any retention policy will be used.\nnode.retentionPolicy(value string)',
                round: 'Optional duration for rounding timestamps. Helpful to ensure data points land on specific boundaries\nnode.round(value time.Duration)',
                truncate: 'Optional duration for truncating timestamps. Helpful to ensure data points land on specific boundaries\nnode.truncate(value time.Duration)',
                where: 'Filter the current stream using the given expression.\nnode.where(lambda ast.LambdaNode)', }),
            new TickNode('GroupByNode', defaultMethods, {
                byMeasurement: 'If set will include the measurement name in the group ID. Along with any other group by dimensions.\nnode.byMeasurement()', }),
            new TickNode('HTTPOutNode', defaultMethods, {}),
            new TickNode('InfluxDBOutNode', influxOutMethods, {
                buffer: 'Number of points to buffer when writing to InfluxDB. Default: 1000\nnode.buffer(value int64)',
                cluster: 'The name of the InfluxDB instance to connect to. If empty the configured default will be used.\nnode.cluster(value string)',
                create: 'Create indicates that both the database and retention policy will be created, when the task is started. If the retention policy name is empty than no retention policy will be specified and the default retention policy name will be created. If the database already exists nothing happens.\nnode.create()',
                database: 'The name of the database.\nnode.database(value string)',
                flushInterval: 'Write points to InfluxDB after interval even if buffer is not full. Default: 10s\nnode.flushInterval(value time.Duration)',
                measurement: 'The name of the measurement.\nnode.measurement(value string)',
                precision: 'The precision to use when writing the data.\nnode.precision(value string)',
                retentionPolicy: 'The name of the retention policy.\nnode.retentionPolicy(value string)',
                tag: 'Add a static tag to all data points. Tag can be called more than once.\nnode.tag(key string, value string)',
                writeConsistency: 'The write consistency to use when writing the data.\nnode.writeConsistency(value string)', }),
            new TickNode('InfluxQLNode', defaultMethods, {
                as: 'The name of the field, defaults to the name of function used (i.e. .mean -> \'mean\')\nnode.as(value string)',
                usePointTimes: 'Use the time of the selected point instead of the time of the batch. Only applies to selector functions like first, last, top, bottom, etc. Aggregation functions always use the batch time.\nnode.usePointTimes()', }),
            new TickNode('JoinNode', defaultMethods, {
                as: 'Prefix names for all fields from the respective nodes. Each field from the parent nodes will be prefixed with the provided name and a \'.\'. See examples in docs.\nnode.as(names ...string)',
                delimiter: 'The delimiter between the As names and existing field an tag keys. Can be the empty string, but you are responsible for ensuring conflicts are not possible if you use the empty string.\nnode.delimiter(value string)',
                fill: 'Fill the data. The fill option implies the type of join: inner or full outer. See the docs.\nnode.fill(value interface{})',
                on: 'Join on a subset of the group by dimensions. This is a special case where you want a single point from one parent to join with multiple points from a different parent. See the docs.\nnode.on(dims ...string)',
                streamName: 'The name of this new joined data stream. If empty the name of the left parent is used.\nnode.streamName(value string)',
                tolerance: 'The maximum duration of time that two incoming points can be apart and still be considered to be equal in time. The joined data point\'s time will be rounded to the nearest multiple of the tolerance duration.\nnode.tolerance(value time.Duration)', }),
            new TickNode('LogNode', defaultMethods, {
                level: 'The level at which to log the data. One of: DEBUG, INFO, WARN, ERROR Default: INFO\nnode.level(value string)',
                prefix: 'Optional prefix to add to all log messages\nnode.prefix(value string)', }),
            new TickNode('QueryNode', defaultMethods, {
                align: 'Align start and stop times for quiries with even boundaries of the QueryNode.Every property. Does not apply if using the QueryNode.Cron property.\nnode.align()',
                alignGroup: 'Align the group by time intervals with the start time of the query\nnode.alignGroup()',
                cluster: 'The name of the InfluxDB instance to connect to. If empty the configured default will be used.\nnode.cluster(value string)',
                cron: 'Define a schedule using a cron syntax. The specific cron implementation is documented here: https://github.com/gorhill/cronexpr#implementation The Cron property is mutually exclusive with the Every property.\nnode.cron(value string)',
                every: 'How often to query InfluxDB.\nnode.every(value time.Duration)',
                fill: 'Fill the data. See the docs.\nnode.fill(value interface{})',
                groupBy: 'Group the data by a set of dimensions. Can specify one time dimension. See the docs.\nnode.groupBy(d ...interface{})',
                groupByMeasurement: 'If set will include the measurement name in the group ID. Along with any other group by dimensions.\nnode.groupByMeasurement()',
                offset: 'How far back in time to query from the current time. See the docs.\nnode.offset(value time.Duration)',
                period: 'The period or length of time that will be queried from InfluxDB\nnode.period(value time.Duration)', }),
            new TickNode('SampleNode', defaultMethods, {}),
            new TickNode('ShiftNode', defaultMethods, {}),
            new TickNode('SampleNode', defaultMethods, {}),
            new TickNode('StatsNode', defaultMethods, {
                align: 'Round times to the StatsNode.Interval value.\nnode.align()' }),
            new TickNode('UnionNode', defaultMethods, {
                align: 'The new name of the stream. If empty the name of the left node (i.e. leftNode.union(otherNode1, otherNode2)) is used.\nnode.rename(value string)' }),
            new TickNode('WhereNode', defaultMethods, {}),
            new TickNode('WindowNode', defaultMethods, {
                align: 'If the align property is not used to modify the window node, then the window alignment is assumed to start at the time of the first data point it receives. If align property is set, the window time edges will be truncated to the every property (For example, if a data point\'s time is 12:06 and the every property is 5m then the data point\'s window will range from 12:05 to 12:10).\nnode.align()',
                every: 'How often the current window is emitted into the pipeline. If equal to zero, then every new point will emit the current window.\nnode.every(value time.Duration)',
                everyCount: 'EveryCount determines how often the window is emitted based on the count of points. A value of 1 means that every new point will emit the window.\nnode.everyCount(value int64)',
                fillPeriod: 'FillPeriod instructs the WindowNode to wait till the period has elapsed before emitting the first batch. This only applies if the period is greater than the every value.\nnode.fillPeriod()',
                period: 'The period, or length in time, of the window.\nnode.period(value time.Duration)',
                periodCount: 'PeriodCount is the number of points per window.\nnode.periodCount(value int64)',
            }),
        ];

        let nodes:TickNodes = {};
        for(let node of nodeList)
            nodes[node.name] = node;

        //==================================================================================================================
        // METHODS
        //==================================================================================================================

        rootMethods['batch'] = new TickMethod(nodes['BatchNode'], 'A node that handles creating several child QueryNodes. Each call to query creates a child batch node that can further be configured. ');
        rootMethods['stream'] = new TickMethod(nodes['StreamNode'], 'A StreamNode represents the source of data being streamed to Kapacitor via any of its inputs. ');

        defaultMethods['alert'] = new TickMethod(nodes['AlertNode'], 'Create an alert node, which can trigger alerts.\nnode|alert()');
        defaultMethods['bottom'] = new TickMethod(nodes['InfluxQLNode'], 'Select the bottom num points for field and sort by any extra tags or fields.\nnode|bottom(num int64, field string, fieldsAndTags ...string)');
        defaultMethods['combine'] = new TickMethod(nodes['CombineNode'], 'Combine this node with itself. The data are combined on timestamp.\nnode|combine(expressions ...ast.LambdaNode)');
        defaultMethods['count'] = new TickMethod(nodes['InfluxQLNode'], 'Count the number of points.\nnode|count(field string)');
        defaultMethods['cumulativeSum'] = new TickMethod(nodes['InfluxQLNode'], 'Compute a cumulative sum of each point that is received. A point is emitted for every point collected.\nnode|cumulativeSum(field string)');
        defaultMethods['deadman'] = new TickMethod(nodes['AlertNode'], 'See documentation for examples.\nnode|deadman(threshold float64, interval time.Duration, expr ...ast.LambdaNode)');
        defaultMethods['default'] = new TickMethod(nodes['DefaultNode'], 'Create a node that can set defaults for missing tags or fields.\nnode|default()');
        defaultMethods['delete'] = new TickMethod(nodes['DeleteNode'], 'Create a node that can delete tags or fields.\nnode|delete()');
        defaultMethods['derivative'] = new TickMethod(nodes['DerivativeNode'], 'Create a new node that computes the derivative of adjacent points.\nnode|derivative(field string)');
        defaultMethods['difference'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the difference between points independent of elapsed time.\nnode|difference(field string)');
        defaultMethods['distinct'] = new TickMethod(nodes['InfluxQLNode'], 'Produce batch of only the distinct points.\nnode|distinct(field string)');
        defaultMethods['elapsed'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the elapsed time between points\nnode|elapsed(field string, unit time.Duration)');
        defaultMethods['eval'] = new TickMethod(nodes['EvalNode'], 'Create an eval node that will evaluate the given transformation function to each data point. A list of expressions may be provided and will be evaluated in the order they are given. The results are available to later expressions.\nnode|eval(expressions ...ast.LambdaNode)');
        defaultMethods['first'] = new TickMethod(nodes['InfluxQLNode'], 'Select the first point.\nnode|first(field string)');
        defaultMethods['flatten'] = new TickMethod(nodes['FlattenNode'], 'Flatten points with similar times into a single point.\nnode|flatten()');
        defaultMethods['groupBy'] = new TickMethod(nodes['GroupByNode'], 'Group the data by a set of tags.\nCan pass literal * to group by all dimensions.\nnode|groupBy(tag ...interface{})');
        defaultMethods['holtWinters'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the holt-winters forecast of a data set.\nnode|holtWinters(field string, h int64, m int64, interval time.Duration)');
        defaultMethods['holtWintersWithFit'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the holt-winters forecast of a data set. This method also outputs all the points used to fit the data in addition to the forecasted data.\nnode|holtWintersWithFit(field string, h int64, m int64, interval time.Duration)');
        defaultMethods['httpOut'] = new TickMethod(nodes['HTTPOutNode'], 'Create an HTTP output node that caches the most recent data it has received. The cached data are available at the given endpoint.\nnode|httpOut(endpoint string)');
        defaultMethods['influxDBOut'] = new TickMethod(nodes['InfluxDBOutNode'], 'Create an influxdb output node that will store the incoming data into InfluxDB.\nnode|influxDBOut()');
        defaultMethods['join'] = new TickMethod(nodes['JoinNode'], 'Join this node with other nodes. The data are joined on timestamp.\nnode|join(others ...Node)');
        defaultMethods['last'] = new TickMethod(nodes['InfluxQLNode'], 'Select the last point.\nnode|last(field string)');
        defaultMethods['log'] = new TickMethod(nodes['LogNode'], 'Create a node that logs all data it receives.\nnode|log()');
        defaultMethods['max'] = new TickMethod(nodes['InfluxQLNode'], 'Select the maximum point.\nnode|max(field string)');
        defaultMethods['mean'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the mean of the data.\nnode|mean(field string)');
        defaultMethods['median'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the median of the data. Note, this method is not a selector, if you want the median point use .percentile(field, 50.0).\nnode|median(field string)');
        defaultMethods['min'] = new TickMethod(nodes['InfluxQLNode'], 'Select the minimum point.\nnode|min(field string)');
        defaultMethods['mode'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the mode of the data.\nnode|mode(field string)');
        defaultMethods['movingAverage'] = new TickMethod(nodes['InfluxQLNode'], 'Compute a moving average of the last window points. No points are emitted until the window is full.\nnode|movingAverage(field string, window int64)');
        defaultMethods['percentile'] = new TickMethod(nodes['InfluxQLNode'], 'Select a point at the given percentile. This is a selector function, no interpolation between points is performed.\nnode|percentile(field string, percentile float64)');
        defaultMethods['sample'] = new TickMethod(nodes['SampleNode'], 'Create a new node that samples the incoming points or batches.\nOne point will be emitted every count or duration specified.\nnode|sample(rate interface{})');
        defaultMethods['shift'] = new TickMethod(nodes['ShiftNode'], 'Create a new node that shifts the incoming points or batches in time.\nnode|shift(shift time.Duration)');
        defaultMethods['spread'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the difference between min and max points.\nnode|spread(field string)');
        defaultMethods['stats'] = new TickMethod(nodes['StatsNode'], 'Create a new stream of data that contains the internal statistics of the node. The interval represents how often to emit the statistics based on real time. This means the interval time is independent of the times of the data points the source node is receiving.\nnode|stats(interval time.Duration)');
        defaultMethods['stddev'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the standard deviation.\nnode|stddev(field string)');
        defaultMethods['sum'] = new TickMethod(nodes['InfluxQLNode'], 'Compute the sum of all values.\nnode|sum(field string)');
        defaultMethods['top'] = new TickMethod(nodes['InfluxQLNode'], 'Select the top num points for field and sort by any extra tags or fields.\nnode|top(num int64, field string, fieldsAndTags ...string)');
        defaultMethods['union'] = new TickMethod(nodes['UnionNode'], 'Perform the union of this node and all other given nodes.\nnode|union(node ...Node)');
        defaultMethods['where'] = new TickMethod(nodes['WhereNode'], 'Create a new node that filters the data stream by a given expression.\nnode|where(expression ast.LambdaNode)');
        defaultMethods['window'] = new TickMethod(nodes['WindowNode'], 'Create a new node that windows the stream by time.\nNOTE: Window can only be applied to stream edges.\nnode|window()');

        batchMethods['deadman'] = defaultMethods['deadman'];
        batchMethods['query'] = new TickMethod(nodes['QueryNode'], 'The query to execute. Must not contain a time condition in the WHERE clause or contain a GROUP BY clause. The time conditions are added dynamically according to the period, offset and schedule. The GROUP BY clause is added dynamically according to the dimensions passed to the groupBy method.\nnode|query(q string)');
        batchMethods['stats'] = defaultMethods['stats'];

        streamMethods['deadman'] = defaultMethods['deadman'];
        streamMethods['from'] = new TickMethod(nodes['FromNode'], 'Creates a new FromNode that can be further filtered using the Database, RetentionPolicy, Measurement and Where properties. From can be called multiple times to create multiple independent forks of the data stream.\nnode|from()');
        streamMethods['stats'] = defaultMethods['stats'];

        influxOutMethods['deadman'] = defaultMethods['deadman'];
        influxOutMethods['stats'] = defaultMethods['stats'];

        let allMethods:TickMethods = {};
        for(let methodName in defaultMethods)
            if(defaultMethods.hasOwnProperty(methodName))
                allMethods[methodName] = defaultMethods[methodName];
        allMethods['query'] = batchMethods['query'];
        allMethods['from'] = streamMethods['from'];
        //allMethods['batch'] = rootMethods['batch'];
        //allMethods['stream'] = rootMethods['stream'];

        return {
            allMethods: allMethods,
            streamMethods: streamMethods,
            batchMethods: batchMethods,
            rootMethods: rootMethods,
            defaultMethods: defaultMethods,
        }
    }
}
