/// <reference types="ace" />

namespace TickSyntax
{
    interface KeywordMapper { }
    interface Terminal
    {
        token:any,
        regex:string|RegExp;
        next?:string;
    }

    export function getKeywords():{[key:string]:any}
    {
        return {
            "keyword": "var lambda",
            "keyword.batch": "batch",
            "keyword.stream": "stream",
            "constant.language": "TRUE FALSE AND OR"
        };
    }

    export function getTokens(keywordMapper:KeywordMapper):{[key:string]:Terminal[]}
    {
        let identifierRe = "[a-zA-Z0-9_]*";
        return {
            "start": [
                { token: "comment",  regex: "//.*$" },
                { token: "string", regex: "'[^']*'" },
                { token: "string.regex", regex: "/(\\\\/|[^/])*/" },
                { token: "support.type", regex: "\"[^\"]*\"" },
                { token: "constant.numeric", regex: "[+-]?\\d+(\\.\\d+)?(u|µ|ms|m|s|h|d|w)?\\b" },
                { token: "keyword.operator", regex: "[-+%=*/&^!><?:~]+" }, // look ma, no escape sequences!
                { token: "paren.lparen", regex: "[\\[({]" },
                { token: "paren.rparen", regex: "[\\])}]" },
                { token: keywordMapper, regex: "\\b\\w+\\b" },

                // these we want to match specially for autocompletion
                { token: ["keyword.operator.function", "text.whitespace.function", "entity.name.function" ], regex: "(\\|)(\\s*)(" + identifierRe + ")", },
                { token: ["keyword.operator.tag", "text.whitespace.tag", "entity.name.tag" ], regex: "(\\.)(\\s*)(" + identifierRe + ")", }, ],
        };
    }
}
