/// <reference types="jquery" />

namespace Influx
{
    const urlInfluxQuery = "/query";

    export interface QueryResponse
    {
        results:QueryResult[];
    }

    export interface QueryResult
    {
        series?:Series[];
        error?:string;
        messages?:string[];
    }

    export interface Series
    {
        name:string;
        tags?:{[tag:string]:string};
        columns:string[];
        values:any[][];
    }

    export function queryJson(query:string, callback:(response:any)=>void, onError?:(url:string, xhr:JQueryXHR)=>void)
    {
        onError = onError || Utils.defaultApiErrorHandler;
        let url = urlInfluxQuery + "?db=graphite&chunked=true&q=" + encodeURIComponent(query);
        $.ajax(url,
        {
            async: true,
            cache:false,
            dataType: "json",
            error(xhr, status, err) { console.log(status); console.log(err); onError(url, xhr); },
            success: callback
        });
    }

    export function queryCsv(query:string, callback:(csv:string)=>void, onError?:(url:string, xhr:JQueryXHR)=>void)
    {
        onError = onError || Utils.defaultApiErrorHandler;
        let url = urlInfluxQuery + "?chunked=true&q=" + encodeURIComponent(query);
        $.ajax(url,
        {
            async: true,
            cache:false,
            headers: { "Accept": "application/csv" },
            dataType: "text",
            error(xhr, status, err) { console.log(status); console.log(err); onError(url, xhr); },
            success: callback
        });
    }
}
