/// <reference types="ace" />
/// <reference path="ticksyntax.ts" />

// I've tried to avoid making many changes to the basic template of languages that are found in:
// https://github.com/ajaxorg/ace/tree/master/lib/ace/mode

// do this so we don't have to worry about pulling in all the packaged for AMD/require js stuff which I don't understand
declare function define(name: string, deps: string[], ready:Function):void;

namespace TickMode
{
    export function defineMode():void
    {
        define("ace/mode/tick",["require","exports","module","ace/mode/text","ace/mode/text_highlight_rules","ace/lib/oop"],
            function(require:any, exports: {[key:string]:any; }, module:any)
            {
                "use strict";

                let oop = require("../lib/oop");
                let TextMode = require("./text").Mode;
                let TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

                let Rules = function() {

                    let keywordMapper = this.createKeywordMapper(TickSyntax.getKeywords(), "text", true, " ");
                    this.$rules = TickSyntax.getTokens(keywordMapper);
                };
                oop.inherits(Rules, TextHighlightRules);

                // I don't know why the order of these is different but I don't want to mess with it since I don't really understand
                // how this oop lib works or why the prototype is different than the initial function.
                let Mode = function() { this.HighlightRules = Rules; };
                oop.inherits(Mode, TextMode);
                (function() {
                    this.lineCommentStart = '//';
                    this.getNextLineIndent = function(state:any, line:any, tab:any) { return this.$getIndent(line); };
                    this.$id = "ace/mode/tick";
                }).call(Mode.prototype);

                exports.Mode = Mode;
            });
    }
}
